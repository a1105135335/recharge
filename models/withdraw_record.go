/***************************************************
 ** @Desc : This file for 代付数据模型
 ** @Time : 2019.04.10 16:45 
 ** @Author : Joker
 ** @File : withdraw_record
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.10 16:45
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type WithdrawRecord struct {
	Id               int
	UserId           int
	MerchantNo       string
	CreateTime       string
	EditTime         string
	Version          int
	SerialNumber     string
	WhOrderId        string
	WhAmount         float64
	WhUserType       string
	WhAccountNo      string
	WhIssuer         string
	WhAccountName    string
	WhAccountType    string
	WhMobileNo       string
	WhBankNo         string
	WhBranchProvince string
	WhBranchCity     string
	WhBranchName     string
	Status           string
	RecordType       string
	NoticeUrl        string `orm:"type(text)"`
	Remark           string
	RecordClass      string
	ApiNoticeUrl     string `orm:"type(text)"`
	Item             string

	UserName string `orm:"-"`
}

func (*WithdrawRecord) TableEngine() string {
	return "INNODB"
}

func (*WithdrawRecord) TableName() string {
	return WithdrawRecordTBName()
}

/* *
 * @Description: 添加代付记录
 * @Author: Joker
 * @Date: 2019-4-10 16:50:16
 * @Param: Merchant
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*WithdrawRecord) InsertWithdrawRecord(w WithdrawRecord) (int, int64) {
	om := orm.NewOrm()

	in, _ := om.QueryTable(WithdrawRecordTBName()).PrepareInsert()
	id, err := in.Insert(&w)
	if err != nil {
		sys.LogError("InsertWithdrawRecord failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}

	return utils.SUCCESS_FLAG, id
}

// 修改充值记录
func (*WithdrawRecord) UpdateWithdrawRecord(w WithdrawRecord) int {
	om := orm.NewOrm()
	_, err := om.QueryTable(WithdrawRecordTBName()).Filter("wh_order_id", w.WhOrderId).Filter("merchant_no", w.MerchantNo).
		Update(orm.Params{
			"version":   orm.ColValue(orm.ColAdd, 1),
			"edit_time": w.EditTime,
			"status":    w.Status,
			"remark":    w.Remark,
		})
	if err != nil {
		sys.LogError("UpdateWithdrawRecord failed to update for:", err)
		return utils.FAILED_FLAG
	}
	return utils.SUCCESS_FLAG
}

// 查询分页
func (*WithdrawRecord) SelectWithdrawRecordListPage(params map[string]interface{}, limit, offset int) (list []WithdrawRecord) {
	om := orm.NewOrm()
	qt := om.QueryTable(WithdrawRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").Limit(limit, offset).All(&list)
	if err != nil {
		sys.LogError("SelectWithdrawRecordListPage failed to query paging for: ", err)
	}
	return list
}

func (*WithdrawRecord) SelectWithdrawRecordPageCount(params map[string]interface{}) int {
	om := orm.NewOrm()
	qt := om.QueryTable(WithdrawRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	cnt, err := qt.Count()
	if err != nil {
		sys.LogError("SelectWithdrawRecordPageCount failed to count for:", err)
	}
	return int(cnt)
}

// 读取单个代付记录
func (*WithdrawRecord) SelectOneWithdrawRecord(id string) (w WithdrawRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(WithdrawRecordTBName()).Filter("id", id).All(&w)
	if err != nil {
		sys.LogError("SelectOneWithdrawRecord failed to select one:", err)
	}
	return w
}

// 读取单个代付记录通过 订单号
func (*WithdrawRecord) SelectOneWithdrawRecordByOrderId(id string) (w WithdrawRecord) {
	om := orm.NewOrm()
	_, err := om.QueryTable(WithdrawRecordTBName()).Filter("wh_order_id", id).All(&w)
	if err != nil {
		sys.LogError("SelectOneWithdrawRecordByOrderId failed to select one:", err)
	}
	return w
}

//根据条件查询所有代付记录
func (*WithdrawRecord) SelectAllWithdrawRecordBy(params map[string]interface{}) (list []WithdrawRecord) {
	om := orm.NewOrm()
	qt := om.QueryTable(WithdrawRecordTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").All(&list)
	if err != nil {
		sys.LogError("SelectAllWithdrawRecordBy failed to query paging for: ", err)
	}
	return list
}

//判断订单是否存在
func (*WithdrawRecord) WithdrawRecordIsExit(orderId string) bool {
	om := orm.NewOrm()
	return om.QueryTable(WithdrawRecordTBName()).Filter("wh_order_id", orderId).Exist()
}
