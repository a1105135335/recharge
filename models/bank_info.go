/***************************************************
 ** @Desc : This file for 存放银行名称与id映射
 ** @Time : 2019.04.14 15:17 
 ** @Author : Joker
 ** @File : bank_info
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.14 15:17
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
)

type BankInfo struct {
	Id            int
	BankId        string //金融机构ID
	BankName      string //金融机构名称
	BankShortName string //金融机构简称
}

func (*BankInfo) TableEngine() string {
	return "INNODB"
}

func (*BankInfo) TableName() string {
	return BankInfoTBName()
}

// 读取单个信息
func (*BankInfo) SelectOneBankInfo(name string) (m BankInfo) {
	om := orm.NewOrm()
	_, err := om.QueryTable(BankInfoTBName()).Filter("bank_name", name).All(&m)
	if err != nil {
		sys.LogError("SelectOneBankInfo failed to select one:", err)
	}
	return m
}
